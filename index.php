<?php
require_once("src/phapper.php");
require("helpers.php");

main();

function main()
{
    $redditClient = new Phapper();
    $posts = $redditClient->apiCall("/r/chicago")->data->children;
    //print_f($posts);exit;

    foreach ($posts as $post)
    {
        $permalink = $post->data->permalink;
        $comments = $redditClient->apiCall($permalink);
        array_shift($comments); // First record is an array of the post's information. We remove it from the array because it is not relevant.
        foreach ($comments[0]->data->children as $comment)
        {
            $comment_exploded = explode(' ', $comment->data->body);
            print_f($comment_exploded);

            foreach ($comment_exploded as $word)
            {
                if (shortOrLongWord($word))
                    continue;
                $cleaned_word = strtolower(clean($word));
                searchTextFile($cleaned_word);
            }
        }
        break;
    }
    unset($permalink, $comments, $comment_exploded, $cleaned_word);
    exit;
}

/**
 * @var char $char_index - The letter is used to choose which words.txt file to search through.
 * @var string $word     - The word being searched for. 
 */
function searchTextFile($cleaned_word)
{
    $first_letter = $cleaned_word[0];
    if(is_numeric($first_letter) || $first_letter == ''){ return false; }
    $file = file_get_contents('words/'.strtolower($first_letter).'_words.txt');
    $words = explode(',', $file);
    $response =  !in_array($cleaned_word, $words) ?  $cleaned_word . " <strong>- Match Not Found!</strong><br>" : false;
    return $response;
}

/*
 * Strips a string of all special characters, spaces, and line breaks.
 */
function clean($string)
{
   $cleaned_string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
   return preg_replace('/[^A-Za-z0-9\-]/', '', $cleaned_string); // Removes special chars.
}

/*
 * Returns TRUE if length of $word(string) is fewer than $min (Default value = 3) or more than $max (Default value = 8).
 *
 */
function shortOrLongWord($word, $min = 3, $max = 8)
{
    if(strlen($word) < $min && strlen($word) > $max)
    {
        return true;
    }
}



// Todo - Create a global array that will hold the text file arrays when they are created in the searchTextFile function. That way the same text file
// is not created as an array multiple times.









